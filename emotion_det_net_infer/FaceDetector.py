import mediapipe as mp
from PIL import Image, ImageDraw, ImageFont
import numpy as np


def find_font_size(text, font, image, target_width_ratio):
    tested_font_size = 100
    tested_font = ImageFont.truetype(font, tested_font_size)
    observed_width, observed_height = get_text_size(text, image, tested_font)
    estimated_font_size = tested_font_size / (observed_width / image.width) * target_width_ratio
    return round(estimated_font_size)


def get_text_size(text, image, font):
    im = Image.new('RGB', (image.width, image.height))
    draw = ImageDraw.Draw(im)
    return draw.textsize(text, font)

class FaceDetector:
    """Класс для инференса нейронной сети для детекции человеческого лица."""

    def __init__(self):
        # MediaPipe solution API
        mp_face_detection = mp.solutions.face_detection

        # Run MediaPipe Face Detection with short range model.
        self.face_detection = mp_face_detection.FaceDetection(
            min_detection_confidence=0.5, model_selection=1
        )

    @staticmethod
    def load_image(path2image):
        """Загружаем картинку

        Parameters
        ----------
        path2image : str
            путь к картинке и датасета

        Returns
        -------
        np.array
            2d np.array представляющий собой картинку считаную Pil
        """
        image = Image.open(path2image)
        image.load()
        image = np.array(image)

        return image

    @staticmethod
    def get_bbox(img_height, img_width, detection):
        """Переводит нормализованные координаты bbox от face detector'a
        из mediapipe в координаты bbox в пикселях изображения

        Parameters
        ----------
        img_height : int
            высота изображения в пикселях
        img_width : int
            ширина изображения в пикселях
        detection : mediapipe detections struct
            структура хранящая детекции лиц людей от face detector

        Returns
        -------
        (height, width, xmin, ymin)
            координаты bbox в пикселях изображения
        """
        bbox = detection.location_data.relative_bounding_box
        height = int(bbox.height * img_height)
        width = int(bbox.width * img_width)
        xmin = int(bbox.xmin * img_width)
        ymin = int(bbox.ymin * img_height)

        return height, width, xmin, ymin

    def detect_faces(self, image_rgb):
        img_height, img_width, channels = image_rgb.shape
        results = self.face_detection.process(image_rgb)

        detections = []
        if results.detections:
            for detection in results.detections:
                height, width, xmin, ymin = self.get_bbox(
                    img_height, img_width, detection
                )

            detections.append((xmin, width, ymin, height))

        return detections

    def load_image_and_detect_faces(self, path2image):
        image_rgb = self.load_image(path2image)
        detections = self.detect_faces(image_rgb)

        return detections, image_rgb

    @staticmethod
    def viz_detections(image_rgb, detections):
        for detection in detections:
            xmin, width, ymin, height = detection

            image_rgb = Image.fromarray(image_rgb)
            draw = ImageDraw.Draw(image_rgb)

            draw.rectangle(
                (xmin, ymin, xmin + width, ymin + height), fill=(0,255,0), width=3
            )

        return image_rgb

    def close(self):
        self.face_detection.close()


class DetectEmotionOnFaces(FaceDetector):
    def __init__(self, emotion_net):
        super().__init__()

        self.emotion_net = emotion_net

    def viz_emo_detections(self, image_rgb, detections):
        for detection in detections:
            xmin, width, ymin, height = detection

            image_rgb = Image.fromarray(image_rgb)
            draw = ImageDraw.Draw(image_rgb)

            draw.rectangle(
                (xmin, ymin, xmin + width, ymin + height), outline=(0,255,0), width=3
            )

            emotion_cls, score = self.emotion_net.predict_on_image(
                image_rgb, return_label=True
            )

            width_ratio = 0.2  # Portion of the image the text width should be (between 0 and 1)
            font_family = "FreeMono.ttf"
            text = f"{emotion_cls}:{score:.2%}"
            
            font_size = find_font_size(text, font_family, image_rgb, width_ratio)
            fnt = ImageFont.truetype(font_family, font_size)
            
            draw.text(
                (xmin, ymin + height),
                text,
                fill=(0, 255, 0),
                font=fnt
            )

        return image_rgb
